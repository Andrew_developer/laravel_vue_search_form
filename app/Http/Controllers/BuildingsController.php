<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Building;
use App\Http\Filters\BuildingsFilter;

class BuildingsController extends Controller
{
    public function index(BuildingsFilter $request)
    {
        $buildings = Building::filter($request)->get();
        return response()->json($buildings);
    }
}
