<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class BuildingsFilter extends QueryFilter
{
    public function name($name)
    {
        $words = array_filter(explode(' ', $name));

        $this->builder->where(function (Builder $query) use ($words) {
            foreach ($words as $word) {
                $query->where('name', 'like', "%$word%");
            }
        });
    }

    public function price($price)
    {
        $this->builder->where('price', '>=', $price);
    }

    public function bathrooms($bathrooms)
    {
        $this->builder->where('bathrooms', strtolower($bathrooms));
    }

    public function bedrooms($name)
    {
        $this->builder->where('bedrooms', strtolower($name));
    }

    public function garages($garages)
    {
        $this->builder->where('garages', strtolower($garages));
    }

    public function storeys($storeys)
    {
        $this->builder->where('storeys', strtolower($storeys));
    }

}